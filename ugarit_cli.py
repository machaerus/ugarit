#!/usr/bin/env python

import time
import textwrap
import sys
import json
from pprint import pprint

from habanero import Crossref, cn
from scihub import SciHub
import requests
from prompt_toolkit import print_formatted_text as print, HTML, prompt

"""

@author: machaerus

TODO:
	- add timeouts
	- implement retries where missing
	- allow to download more than 20 items

"""


class Ugarit():

	def __init__(self):

		# constants
		self.TRIALS = 3

		# init
		self.sh = SciHub()
		self.cr = Crossref()

	def crossref_search(self, query, offset, limit):
		print("Searching...")
		trial = 0
		while trial < self.TRIALS:
			try:
				r = self.cr.works(query=query, offset=offset, limit=limit)
			except Exception:
				print("Error while calling API. Retrying...")
				trial += 1
				time.sleep(1)
				continue
			if r['status'] == "ok":
				results = r['message']['items']
				results = [item for item in results if
								"DOI" in item.keys() and
								"title" in item.keys() and
								"author" in item.keys()]
				if offset == 0:
					self.search_results = results
				else:
					self.search_results += results
				return True
			else:
				print("Incorrect server response. Retrying...")
				trial += 1
				time.sleep(1)
				continue
		print("Search failed. Something's broken.")
		return False

	def set_doi(self, doi):
		self.doi = doi

	def get_doi(self, n):
		self.doi = self.search_results[n]['DOI']
		return self.doi

	def build_filename(self, n):
		article = self.search_results[n]
		title = article['title'][0]
		author = ""
		for a in article['author']:
			author += self.get_author_name(a)
		author = author[:-2]

		return "{} - {}.pdf".format(author, title)

	def get_author_name(self, author_dict):
		try:
			return "{}, {}; ".format(author_dict['family'], author_dict['given'])
		except KeyError:
			pass  # fallback to "name"
		try:
			return "{}; ".format(author_dict['name'])
		except KeyError:
			return "{}; ".format(author_dict)

	def split_into_lines(self, text, line_len):
		return textwrap.wrap(str(text), line_len)

	def print_search_results(self, offset=0):
		print("Number of articles found: {}".format(len(self.search_results)))
		# print()
		# json.dumps(self.search_results, indent=4)
		# pprint(self.search_results)
		for i, article in enumerate(self.search_results[offset:]):

			title = article['title'][0]

			try:
				author = ""
				for a in article['author']:
					author += self.get_author_name(a)
				author = author[:-2]
			except Exception as e:
				print(article)
				raise

			if article['issued']['date-parts'][0][0] is not None:	
				date = article['issued']['date-parts'][0][0]
			else:
				date = article['created']['date-parts'][0][0]

			info_to_display = {
				"Title": title,
				"Author": author,
				"Date": date,
				"Publisher": article['publisher'],
				"Type": article['type'],
			}

			line_len = 60
			print("[#{}]".format(i + offset + 1))
			for k, v in info_to_display.items():
				v_lines = self.split_into_lines(v, line_len)
				print(" |    {:20}{}".format("{}:".format(k), v_lines[0]))
				for v_line in v_lines[1:]:
					print(" |    {:20}{}".format("{}".format(""), v_line))
			print()

	def unpaywall_check(self, doi):
		try:
			r = requests.get("https://api.oadoi.org/v2/{}?email=some_user@hotmail.com".format(doi))
			response = r.json()
			return response['is_oa']
		except KeyError as e:
			print("Invalid response from Unpaywall!")
			print(response)
			raise e

	def unpaywall_download(self, doi, save="test2.pdf"):
		# https://stackoverflow.com/questions/15644964/python-progress-bar-and-downloads
		r = requests.get("https://api.oadoi.org/v2/{}?email=some_user@hotmail.com".format(doi))
		response = r.json()
		for loc in response['oa_locations']:
			if loc['url_for_pdf']:
				dl = requests.get(loc['url_for_pdf'])
				with open(save, 'wb') as output:
					output.write(dl.content)
				return True
			else:
				continue
		return False

	def scihub_download(self, doi, save="text2.pdf"):
		try:
			dl = self.sh.download(doi)
		except Exception as e:
			print(e)
			return False
		return True

	def crossref_get_citation(self, doi):
		return cn.content_negotiation(ids=doi, format="bibentry")


def main():

	ugarit = Ugarit()

	print()
	print(HTML("<b><skyblue>Search for a new article</skyblue></b>"))
	print("-------------------------")
	print()

	query = prompt("{:20}".format("Query:"))

	print()

	limit = 3
	offset = 0
	shown_articles = 0
	while True:
		ugarit.crossref_search(query, offset, limit)
		ugarit.print_search_results(shown_articles)
		print()
		load_more = prompt("Load more results? [Y/n] ")
		if load_more == "n":
			break
		shown_articles = len(ugarit.search_results)
		offset += limit
		continue

	print()

	article_no = prompt("Select article: ")
	doi = ugarit.get_doi(int(article_no) - 1)

	print("[1] Download item")
	print("[2] Download citation (BibTeX)")
	action_download = prompt("What do you want to do? ")

	if action_download == "1":

		print()
		print("Checking Unpaywall...")
		check_unpaywall = ugarit.unpaywall_check(doi)
		if check_unpaywall:
			action = prompt("Article found on Unpaywall. Do you want to download? [y/n] ")
			if action == "y":
				print("Downloading (a little patience please)...")
				filename = ugarit.build_filename(int(article_no) - 1)
				ugarit.unpaywall_download(doi, save=filename)
				print(filename)
				print("Done!")
			elif action == "n":
				print("Kthxbai!")
		else:
			action = prompt("Article is not accessible for free. Do you want to try SciHub? [y/n] ")
			if action == "y":
				print("Downloading (a little patience please)...")
				filename = ugarit.build_filename(int(article_no) - 1)
				if ugarit.scihub_download(doi, save=filename):
					print(filename)
					print("Done!")
				else: 
					print("Sorry, can't find it!")
			elif action == "n":
				print("Kthxbai!")

	elif action_download == "2":

		print()
		print("Downloading citation data...")
		citation = ugarit.crossref_get_citation(doi)
		print()
		print(citation)
		print()


if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print(HTML("\n<b>Bye!</b>"))
		sys.exit()